import { CommonRoutesConfig } from "../commons/common.routes.config";
import express = require('express');
import UsersController from '../controllers/users.controller';
import { handleError } from "../middlewares/error-handler.middlware";

export class UsersRoutes extends CommonRoutesConfig {

    static API_URI: string = `${CommonRoutesConfig.API_URI}/users`; 

    constructor(app: express.Application) {
        super(app, 'UserRoutes');
    }

    configureRoutes(): express.Application {
        this.app.get(`${UsersRoutes.API_URI}`, UsersController.listUsers);
        this.app.get(`${UsersRoutes.API_URI}/error`, UsersController.error);
        this.app.use(handleError);
        return this.app;
    }
}