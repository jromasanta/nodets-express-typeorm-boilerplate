import "reflect-metadata";
import {createConnection} from "typeorm";
import express = require('express');
import helmet = require('helmet');
import cors = require('cors');
import debug = require('debug');
import { CommonRoutesConfig } from "./commons/common.routes.config";
import { UsersRoutes } from "./routes/user.routes";

createConnection().then(async connection => {

    const app: express.Application = express();
    const debugLog: debug.IDebugger = debug('app');
    const port: number = 3000;

    app.use(helmet());
    app.use(express.json());
    app.use(cors());

    const routes: Array<CommonRoutesConfig> = [];
    routes.push(new UsersRoutes(app));

    app.listen(port, () => {
        debugLog(`Server running at http://localhost:${port}`);
        routes.forEach((route: CommonRoutesConfig) => {
            route.configureRoutes();
            debugLog(`Routes configured for ${route.getName()}`);
        });
    });

}).catch(error => console.log(error));
