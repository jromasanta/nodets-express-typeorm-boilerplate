import { Request, Response, NextFunction } from "express";

class UsersController {
    async listUsers(req: Request, res: Response, next: NextFunction) {
        try {
            res.status(200).json({ message: 'List of users' })
        } catch (error) {
            next(error);
        }
    }

    async error(req: Request, res: Response, next: NextFunction) {
        try {
            // res.status(200).json({ message: 'List of users' })
            throw new Error('bithc');
        } catch (error) {
            next(error);
        }
    }
}

export default new UsersController();