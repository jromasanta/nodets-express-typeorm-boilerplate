import express = require('express');
import { handleError } from '../middlewares/error-handler.middlware';

export abstract class CommonRoutesConfig {

    static API_URI: string = '/api';
    app: express.Application
    name: string;

    constructor(app: express.Application, name: string) {
        this.app = app;
        this.name = name;
    }

    getName(): string {
        return this.name;
    }

    abstract configureRoutes(): express.Application;
}